﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FakeNews.Model
{
    public class NewsItem
    {

        public int Id { get; set; }
        public string Category { get; set; }
        public string Headline { get; set; }
        public string Subhead { get; set; }
        public string Dataline { get; set; }
        public string Image { get; set; }
    }
    public class NewManager
    {
        public static void GetNews(string category,
            ObservableCollection<NewsItem> newsItems)
        {
            var allItems =  getNewsItems();

            var filteredNewsItems = allItems.Where(p => p.Category == category).ToList();

            newsItems.Clear();

            filteredNewsItems.ForEach(p => newsItems.Add(p));

        }
        private static List<NewsItem> getNewsItems()
        {
            var items = new List<NewsItem>();

            // items 01
            items.Add(new NewsItem()
            {
                Id = 1,
                Category = "Financial",
                Headline = "loren Ipsum",
                Subhead = "doro sit amet",
                Dataline = "Nunc tristique nec",
                Image = "Assets/image2/Financia1.png"
            });

            //Items 02
            items.Add(new NewsItem()
            {
                Id = 2,
                Category = "Financial",
                Headline = "Etiam ac felis viverra",
                Subhead = "vulputate nisl ac, aliquet nisi",
                Dataline = "tortor porttitor, eu fermentum ante congue",
                Image = "Assets/image2/Financia2.png"
            });

            //Items 03
            items.Add(new NewsItem()
            {
                Id = 3,
                Category = "Financial",
                Headline = "Integer sed turpis erat",
                Subhead = "sed turpis lorem, quis iterdum dolor",
                Dataline = "in viverra metus facilisis sed",
                Image = "Assets/image2/Financia3.png"
            });

            return items;
        }
    }
}

