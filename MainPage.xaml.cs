﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using FakeNews.Model;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace FakeNews
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private ObservableCollection<NewsItem> NewsItems;
        public MainPage() {
            this.InitializeComponent();
            NewsItems = new ObservableCollection<NewsItem>();
    }
   
        private void HamburgerButton_Click(Object sender, RoutedEventArgs e)
        {
            MysplitView.IsPaneOpen = !MysplitView.IsPaneOpen;
        }
        private void ListBox_selectionChanged(object sender, SearchBoxQueryChangedEventArgs e)
        {
            if(Financial.IsSelected)
            {
                NewManager.GetNews("Financial", NewsItems);
                TitleTexBlock.Text = "Financial";
            }
            else if (Food.IsSelected)
            {
                NewManager.GetNews("Food", NewsItems);
                TitleTexBlock.Text = "Food";
            }
        }
        private void Page_Loaded(Object sender, RoutedEventArgs e)
        {
            Financial.IsSelected = true;
        }
    }
}
